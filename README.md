Panda 进销存系统
===============

当前最新版本： 1.1.7（发布日期：2020-11-20）


介绍
-----------------------------------

熊猫云仓存是一款面向中小企业的供销链管理系统，基于J2EE快速开发平台Spring-Boot开发，采用前后端分离架构：SpringBoot2.x，Mybatis-plus，Shiro，JWT。项目基于十多年的中小企业ERP管理经验，实现仓库进销存绝大多数功能，也可手工加入复杂的业务逻辑！
 - 基础版：简单的仓储进销存管理系统，能对商品种类、商品库存、采购、货位、入库、出库、盘点等基本管理。
 - 标准版：接受定制
 - 本发布为基础版


功能模块
-----------------------------------
```
┌─库存管理
│  ├─入库管理
│  │  ├─商品入库
│  │  ├─新增仓库
│  │  ├─仓库管理
│  │  └─其他入库
│  ├─出库管理
│  │  ├─商品出库
│  │  ├─销售退货入库
│  │  ├─盘亏出库
│  │  └─其他出库
│  ├─库存调拨
│  ├─成本调整
│  ├─库存盘点
│  ├─实时库存
│  └─库存报表（开发中）
├─基础资料 
│  ├─客户、供应商
│  ├─仓库、物料分类、物料、计量单位
│  └─银行账户、币种
├─系统管理（Jeecg-Boot功能）
│  ├─用户管理
│  ├─角色管理
│  ├─菜单管理
│  ├─权限设置（支持按钮权限、数据权限）
│  ├─表单权限（控制字段禁用、隐藏）
│  ├─部门管理
│  ├─字典管理
│  ├─系统公告
│  ├─我的组织机构
│  ├─职务管理
│  └─通讯录
├─消息中心（Jeecg-Boot）
│  ├─消息管理
│  └─模板管理
├─智能化开发支持（Jeecg-Boot）
│  ├─代码生成器功能（一键生成前后端代码，生成后无需修改直接用，绝对是后端开发福音）
│  ├─代码生成器模板（提供4套模板，分别支持单表和一对多模型，不同风格选择）
│  ├─代码生成器模板（生成代码，自带excel导入导出）
│  ├─查询过滤器（查询逻辑无需编码，系统根据页面配置自动生成）
│  ├─高级查询器（弹窗自动组合查询条件）
│  ├─Excel导入导出工具集成（支持单表，一对多 导入导出）
│  └─平台移动自适应支持
└──├─性能扫描监控
   ├─定时任务
   ├─系统日志
   ├─消息中心（支持短信、邮件、微信推送等等）
   ├─数据日志（记录数据快照，可对比快照，查看数据变更情况）
   ├─系统通知
   ├─SQL监控
   └─swagger-ui(在线接口文档)
```
   

 
技术架构
-----------------------------------
#### 开发环境

- 语言：Java 8

- IDE(JAVA)： IDEA / Eclipse，安装lombok插件
 
- IDE(前端)：  IDEA / WebStorm

- 依赖管理：Maven

- 数据库：MySQL5.7

- 缓存：Redis


#### 后端

- 基础框架：Spring Boot 2.1.3.RELEASE

- 持久层框架：Mybatis-plus_3.1.2

- 安全框架：Apache Shiro 1.4.0，Jwt_3.7.0

- 数据库连接池：阿里巴巴Druid 1.1.10

- 缓存框架：redis

- 日志打印：logback

- 其他：fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。


#### 前端
 
- [Vue 2.6.10](https://cn.vuejs.org/),[Vuex](https://vuex.vuejs.org/zh/),[Vue Router](https://router.vuejs.org/zh/)
- [Axios](https://github.com/axios/axios)
- [ant-design-vue](https://vuecomponent.github.io/ant-design-vue/docs/vue/introduce-cn/)
- [webpack](https://www.webpackjs.com/),[yarn](https://yarnpkg.com/zh-Hans/)
- [vue-cropper](https://github.com/xyxiao001/vue-cropper) - 头像裁剪组件
- [@antv/g2](https://antv.alipay.com/zh-cn/index.html) - Alipay AntV 数据可视化图表
- [Viser-vue](https://viserjs.github.io/docs.html#/viser/guide/installation)  - antv/g2 封装实现
- eslint，[@vue/cli 3.2.1](https://cli.vuejs.org/zh/guide)
- vue-print-nb - 打印




后台开发环境和依赖
----
- java
- maven
- jdk8
- mysql
- redis
- 数据库脚本：jeecg-boot\db\jeecgboot&psi_mysql5.7.sql
- 初始系统管理员： admin/123456


前端开发环境和依赖
----
- node
- yarn
- webpack
- eslint
- @vue/cli 3.2.1
- [ant-design-vue](https://github.com/vueComponent/ant-design-vue) - Ant Design Of Vue 实现
- [vue-cropper](https://github.com/xyxiao001/vue-cropper) - 头像裁剪组件
- [@antv/g2](https://antv.alipay.com/zh-cn/index.html) - Alipay AntV 数据可视化图表
- [Viser-vue](https://viserjs.github.io/docs.html#/viser/guide/installation)  - antv/g2 封装实现
- [jeecg-boot-angular 版本](https://gitee.com/dangzhenghui/jeecg-boot)


系统效果
----
##### 系统登录
![系统登录](https://images.gitee.com/uploads/images/2020/1126/161456_96e65a30_605238.jpeg "系统登录.jpg")
![用户注册](https://images.gitee.com/uploads/images/2020/1126/161221_a5e62469_605238.jpeg "resiter.jpg")

##### 应用首页
![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/162122_ae2dd3c5_605238.jpeg "首页.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/162148_64e5134a_605238.jpeg "应用.jpg")

##### 商品出入库
![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/162020_7e30a102_605238.jpeg "商品入库.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/162046_19c2395d_605238.jpeg "商品出库.jpg")

##### 商品管理
![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/173558_3747e6a2_605238.jpeg "商品.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/172755_90240d83_605238.jpeg "商品新增.jpg")

##### 个人权限
![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/172914_b781d503_605238.jpeg "我的.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/1126/172927_794ef60b_605238.jpeg "权限.jpg")

其他说明
----


